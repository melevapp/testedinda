//
//  ExecuteBallance.m
//  teste
//
//  Created by Aloisio Mello on 4/11/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import "ExecuteBallance.h"

@implementation ExecuteBallance

-(void)executeBallanceWithContasFileName:(NSString*)contasFileName withTransacoesFileName:(NSString*)transacoesFileName{    
    ContasLista * contaLista = [[ContasLista alloc] initWithFileName:contasFileName];
    TransacoesLista * transacoesLista = [[TransacoesLista alloc] initWithFileName:transacoesFileName];
    
    for(Transacoes * transact in transacoesLista.arrTransaction){
        [contaLista executeTransactionWithTransacoesObject:transact];
    }
    
    for(Contas * contas in contaLista.arrContas){
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        formatter.locale = [NSLocale currentLocale];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        float centsConversion = (float)contas.saldo / 100.0f;
        NSString * formatedBalance = [formatter stringFromNumber:[NSNumber numberWithFloat:centsConversion]];
        NSLog(@"%d,%d",contas.idConta,contas.saldo);
        NSLog(@"Saldo final de %@ na conta: %d",formatedBalance,contas.idConta);
    }
}
@end
