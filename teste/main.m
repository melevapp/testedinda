//
//  main.m
//  teste
//
//  Created by Aloisio Mello on 4/7/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExecuteBallance.h"
#import "FileUtils.h"
#import "NSString+Enhancements.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
    // insert code here...
        if(argc == 3){
            NSString * contasFileName = [NSString stringWithUTF8String:argv[1]];
            NSString * transacoesFileName = [NSString stringWithUTF8String:argv[2]];
            
            FileUtils * fu = [FileUtils alloc];
            
            BOOL fileExistsConta = [fu doesFileExists:contasFileName];
            BOOL fileExistsTransaction = [fu doesFileExists:transacoesFileName];
            
            if(fileExistsConta && fileExistsTransaction){
                ExecuteBallance * ballance = [ExecuteBallance alloc];
                [ballance executeBallanceWithContasFileName:contasFileName withTransacoesFileName:transacoesFileName];
            }else{
                if(!fileExistsConta)
                    NSLog(@"Não foi possível encontrar o arquivo de contas");
                
                if(!fileExistsTransaction)
                    NSLog(@"Não foi possível encontrar o arquivo de transações");
            }
        }else{
            NSLog(@"Erro ao entrar o parametro de arquivo de Contas");
        }
    }
    return 0;
}
