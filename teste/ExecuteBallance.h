//
//  ExecuteBallance.h
//  teste
//
//  Created by Aloisio Mello on 4/11/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContasLista.h"
#import "Contas.h"
#import "Transacoes.h"
#import "TransacoesLista.h"

@interface ExecuteBallance : NSObject

-(void)executeBallanceWithContasFileName:(NSString*)contasFileName withTransacoesFileName:(NSString*)transacoesFileName;

@end
