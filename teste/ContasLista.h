//
//  ContasLista.h
//  teste
//
//  Created by Aloisio Mello on 4/8/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contas.h"
#import "Transacoes.h"

@interface ContasLista : NSObject

@property(nonatomic,strong) NSMutableArray * arrContas;

-(id)initWithFileName : (NSString*)file;
-(void)executeTransactionWithTransacoesObject:(Transacoes*)transacoes;

@end
