//
//  TransacoesLista.h
//  teste
//
//  Created by Aloisio Mello on 4/8/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Transacoes.h"

@interface TransacoesLista : NSObject

@property(nonatomic,strong) NSMutableArray * arrTransaction;

-(id)initWithFileName : (NSString*)file;

@end
