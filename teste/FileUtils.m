//
//  FileUtils.m
//  teste
//
//  Created by Aloisio Mello on 4/12/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import "FileUtils.h"

@implementation FileUtils

-(BOOL)doesFileExists : (NSString*)filename{
    if([[NSFileManager defaultManager] fileExistsAtPath:filename]){
        return YES;
    } else {
        return NO;
    }
}

@end
