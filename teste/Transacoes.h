//
//  Transacoes.h
//  teste
//
//  Created by Aloisio Mello on 4/8/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Transacoes : NSObject

@property(nonatomic) int idConta;
@property(nonatomic) int saldo;

-(id)initWithString : (NSString*)string;

@end
