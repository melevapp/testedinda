//
//  FileUtils.h
//  teste
//
//  Created by Aloisio Mello on 4/12/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtils : NSObject

-(BOOL)doesFileExists : (NSString*)filename;

@end
