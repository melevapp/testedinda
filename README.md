# README #

### Como executar o projeto? ###

* Clone o projeto em seu repositorio utilizando: 
git clone https://alowez@bitbucket.org/alowez/testedinda.git

* Abra o projeto "teste.xcodeproj" no XCode (projeto criado utilizando XCode 7.3)

* Compile o projeto pressionando as teclas "cmd+b"

### Para executar o projeto em linha de comando ###

* No XCode no grupo "Products" copie o arquivo "teste" para o local que deseja executar o projeto.

* O Formato dos arquivos de Conta e transações devem seguir os padrões especificados nos casos de uso.